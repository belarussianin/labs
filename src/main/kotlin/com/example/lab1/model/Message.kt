package com.example.lab1.model

data class Message(
    val content: String
)
