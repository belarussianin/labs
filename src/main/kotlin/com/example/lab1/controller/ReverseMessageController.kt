package com.example.lab1.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class ReverseMessageController {

    @GetMapping("/reverse")
    fun reverse(
        model: Model,
        @RequestParam(value = "first", defaultValue = "") first: String,
        @RequestParam(value = "second", defaultValue = "") second: String
    ): String {
        model.addAttribute("first", second.reversed())
        model.addAttribute("second", first.reversed())
        return "/reverse"
    }
}